# Taller de hydrasynths

Hydra es una plataforma de creación de visuales en vivo, tiene múltiples funcionalidades, permitiendo correr sin instalar nada.


## Ecosistema de hydra

Hydra puede funcionar de diversas maneras, en el editor `atom` o en un navegador web.

Por facilidad, en este taller se va a trabajar con hydra desde el navegador.

![Hydra en un navegador web](imgs/hydra_navegador.png "Hydra en el navegador")

Hydra puede ejecutarse en linea en este [link](https://hydra-editor.glitch.me/)

**Botones de hydra**

En la parte de arriba a la derecha nos encontramos con 4 íconos.

![Botones de Hydra](imgs/botones.png "Botones de hydra")

De izquierda a derecha:

* **Subir a galería**: Permite subir el sketch creado a la cuenta de twitter [@hydra\_patterns](https://twitter.com/hydra_patterns).
* **Borrar**: Borra el sketch en su totalidad.
* **Generar al azar**: Genera un sketch de manera aleatoria.
* **Ayuda**: Muestra un cuadro de ayuda con instrucciones de uso.

**Editor**

Hydra Maneja su propio editor de textos, con diversas funcionalidades: cortar/copiar/pegar texto, deshacer/rehacer, seleccionar, etc;

Aqui es donde se escribe el código para la generación de visuales y desde aqui se ejecuta el mismo.

![Editor de Hydra](imgs/editor.png "Editor de hydra")

**Consola**

Existe una consola que muestra el código ejecutado y además muestra errores, puede ser relevante al momento de corregir un código.

![Consola de Hydra](imgs/consola.png "Consola de hydra")

## Como empezar

Se escribe el código en el editor, para ejecutar se presiona `Ctrl + Enter ` para ejecutar la linea en la que estamos ó `Ctrl + Shift + Enter` para ejecutar todo el código.

`osc().out)`

![Como empezar](imgs/como_empezar.png "Como empezar en hydra")

hydra funciona de manera modular, cada módulo puede agregarsele un nuevo módulo, así los efectos pueden sumarse.

Por ejemplo:

` osc().rotate().pixelate().out()`

1.- genera una oscilación.

2.- rota.

3.- pixelea.

![Ejemplo de la programación modular de Hydra](imgs/modulos.png "Ejemplo de la programación modular de hydra")

El orden es muy importante, se codifica de izquierda a derecha, por ejemplo esto dará resultados distintos:

`osc().pixelate().rotate().out()`

![Ejemplo de orden en la programación modular en Hydra](imgs/modulos2.png "Ejemplo del orden en la programación modular de hydra")


1.- genera una oscilación.

2.- pixelea.

3.- rota.

## Generadores

Son los que generan los visuales inicialmente, a partir de estos se suelen agregar más módulos.

**Osciladores**

Genera oscilaciones de colores.

`osc(frecuencia, velocidad, color)`

Por ejemplo:

`osc(1,1,1).out()`

generaría una oscilación con una frecuencia = 1, velocidad = 1 y color = 1.

![Ejemplo de osc](imgs/osc.png "Ejemplo de uso de osc")

`osc(4,1,1).out()`

generaría una oscilación con una frecuencia = 4, velocidad = 1 y color = 1.

![Ejemplo de osc](imgs/osc2.png "Ejemplo de uso de osc")

`osc(4,1,10).out()`

generaría una oscilación con una frecuencia = 4, velocidad = 1 y color = 10.

![Ejemplo de osc](imgs/osc3.png "Ejemplo de uso de osc")

**webcam**

Obtiene imágenes en vivo de la cámara web.

Es necesario inicializar primero la cámara y aceptar los permisos.

`s0.initCam()`

![initCam](imgs/initcam.png "Inicializando cámara")

y usar la fuente con src(s0)

`src(s0).out()`

![usando la webcam](imgs/s0.png "Usando la webcam")

**Figuras geométricas**

Genera figuras geométricas de *n* lados.

`shape(nlados)`

Por ejemplo:

`shape(3).out()`

![Ejemplo de shape](imgs/shape.png "Ejemplo de uso de shape")

Esto generaría un triangulo.

`shape(100).out()`

![Ejemplo de shape](imgs/shape2.png "Ejemplo de uso de shape")

Esto generaría una figura de 100 lados.

**Ruido**

Genera ruido de color gris.

`noise(cantidad, velocidad)`

Por ejemplo:

`noise(1,1).out()`

![Ejemplo de noise](imgs/noise.png "Ejemplo de uso de noise")

`noise(10,1).out()`

![Ejemplo de noise](imgs/noise2.png "Ejemplo de uso de noise")


**Voronoi**

Genera teselaciones de voronoi.

`voronoi(escala, velocidad, mezclado)`

Por ejemplo:

`voronoi(5,1,0).out()`

![Ejemplo de voronoi](imgs/voronoi.png "Ejemplo de uso de voronoi")

`voronoi(5,1,2).out()`

![Ejemplo de voronoi](imgs/voronoi2.png "Ejemplo de uso de voronoi")

## Aritmética

Es posible unir dos generadores entre sí usando lógicas aritméticas.

**sumar**

Suma de dos generadores

`generador1.add(generador2)`

 donde `generado1` y `generador2` pueden ser cualquiera de los antes mencionados.

Por ejemplo:

`osc(1,1,10).add( noise(1,1) ).out()`

![Ejemplo de suma de generadores](imgs/add.png "Ejemplo de suma de generadores")

**resta**

Resta de dos generadores

`generador1.diff(generador2)`

 donde `generado1` y `generador2` pueden ser cualquiera de los antes mencionados.

Por ejemplo:

`osc(1,1,10).diff( noise(1,1) ).out()`

![Ejemplo de resta de generadores](imgs/diff.png "Ejemplo de resta de generadores")

**multiplicación**

multiplicación de dos generadores

`generador1.mult(generador2)`

 donde `generado1` y `generador2` pueden ser cualquiera de los antes mencionados.

Por ejemplo:

`osc(1,1,10).mult( noise(1,1) ).out()`

![Ejemplo de multiplicación de generadores](imgs/mult.png "Ejemplo de multiplicación de generadores")

## Efectos

Son efectos que se agregan a los generadores y hay de distinto tipo.

### Efectos geométricos

**escalado**

Escala un generador

`generador.scale(py, px).out()`

`generador` puede ser cualquiera de los antes mencionados.

`px` y `py` es la razón de escalamiento en x y en y.

Por ejemplo:

`osc(3,1,40).scale(0.012).out()`

![Ejemplo de escalamiento de generadores](imgs/scale.png "Ejemplo de escalamiento de generadores")

`shape(6).scale(1,3).out()`

![Ejemplo de escalamiento de generadores](imgs/scale2.png "Ejemplo de escalamiento de generadores")

**rotación**

Rota un generador

`generador.rotate(angulo).out()`

`generador` puede ser cualquiera de los antes mencionados.

Los ángulos están en radianes.

Por ejemplo:

`osc(4,1,10).rotate().out()`

![Ejemplo de rotación de generadores](imgs/rotate.png "Ejemplo de rotación de generadores")

**repetir**

Repite un generador.

`generador.repeat(nx, ny).out()`

`nx` y `ny` son el número de veces que se va a repetir en x y en y, solo se escribe un valor, se repite tanto en x como en y.

`shape(3).repeat(3).out()`

![Ejemplo de repetición de generadores](imgs/repeat.png "Ejemplo de repetición de generadores")

También existe `.repeatX()` y `.repeatY()` que funcionan de la misma manera, especificamente para x y para y.

`shape(3).repeatX(40).out()`

![Ejemplo de repetición de generadores](imgs/repeat2.png "Ejemplo de repetición de generadores")

**kaleid**

Transforma al generador en un kaleidoscopio

`generador.kaleid(n)`

donde `n` es el número de lados del caleidoscopio.

`voronoi(3).kaleid(5).out()`

![Ejemplo de caleidoscopio con generadores](imgs/kaleid.png "Ejemplo de caleidoscopio con generadores")

