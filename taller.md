# Taller de hydrasynths

Hydra es una plataforma de creación de visuales en vivo, tiene múltiples funcionalidades, permitiendo correr sin instalar nada.


## Ecosistema de hydra

Hydra puede funcionar de diversas maneras, en el editor `atom` o en un navegador web.

Por facilidad, en este taller se va a trabajar con hydra desde el navegador.

![Hydra en un navegador web](imgs/hydra_navegador.png "Hydra en el navegador")

Hydra puede ejecutarse en linea en este [link](https://hydra-editor.glitch.me/)

**Botones de hydra**

En la parte de arriba a la derecha nos encontramos con 4 íconos.

![Botones de Hydra](imgs/botones.png "Botones de hydra")

De izquierda a derecha:

* **Subir a galería**: Permite subir el sketch creado a la cuenta de twitter [@hydra\_patterns](https://twitter.com/hydra_patterns).
* **Borrar**: Borra el sketch en su totalidad.
* **Generar al azar**: Genera un sketch de manera aleatoria.
* **Ayuda**: Muestra un cuadro de ayuda con instrucciones de uso.

**Editor**

Hydra Maneja su propio editor de textos, con diversas funcionalidades: cortar/copiar/pegar texto, deshacer/rehacer, seleccionar, etc;

Aqui es donde se escribe el código para la generación de visuales y desde aqui se ejecuta el mismo.

![Editor de Hydra](imgs/editor.png "Editor de hydra")

**Consola**

Existe una consola que muestra el código ejecutado y además muestra errores, puede ser relevante al momento de corregir un código.

![Consola de Hydra](imgs/consola.png "Consola de hydra")

## Generadores

**Osciladores**

Genera oscilaciones de colores.

`osc(frecuencia, velocidad, color)`

**webcam**

Obtiene imágenes en vivo de la cámara web.

**Figuras geométricas**

Genera figuras geométricas de *n* lados.

**Ruido**

Genera ruido de color gris.

`noise()`

**Voronoi**

Genera teselaciones de voronoi.

## Efectos

Son efectos que se agregan a los generadores.

### Efectos geométricos

**kaleid**

Transforma al generador en un kaleidoscopio

`.kaleid(num)`

